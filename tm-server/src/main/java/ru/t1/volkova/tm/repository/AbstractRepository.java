package ru.t1.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.IRepository;
import ru.t1.volkova.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> records = new ArrayList<>();

    @Nullable
    @Override
    public M add(@NotNull final M model) {
        records.add(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        records.addAll(models);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        records.clear();
        return add(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return records;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return records
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public void removeAll() {
        records.clear();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return records
                .stream()
                .filter(r -> id.equals(r.getId()))
                .findFirst().orElse(null);
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return records.get(index);
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final M model) {
        records.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public void removeAll(@Nullable final List<M> models) {
        if (models == null) return;
        records.removeAll(models);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null) return null;
        @NotNull final M model = findOneByIndex(index);
        removeOne(model);
        return model;
    }

    @NotNull
    @Override
    public Integer getSize() {
        return records.size();
    }

}
