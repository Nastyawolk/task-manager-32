package ru.t1.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.IUserOwnedRepository;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        return records
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @NotNull final Comparator<M> comparator
    ) {
        if (userId == null) return null;
        @Nullable final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        return records
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .filter(r -> id.equals(r.getId()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        return records
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .skip(index)
                .findFirst().orElse(null);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null) return -1;
        return (int) records
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .count();
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @NotNull final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        add(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeOneById(userId, model.getId());
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        removeAll(findAll(userId));
    }

}
