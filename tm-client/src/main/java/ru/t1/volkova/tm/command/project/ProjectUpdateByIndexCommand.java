package ru.t1.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.project.ProjectUpdateByIndexRequest;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Update project by index.";

    @NotNull
    private static final String NAME = "project-update-by-index";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(index, name, description);
        getProjectEndpoint().updateProjectByIndex(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
