package ru.t1.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.project.ProjectGetByIndexRequest;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Show project by index.";

    @NotNull
    private static final String NAME = "project-show-by-index";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(index);
        @Nullable final Project project = getProjectEndpoint().getProjectByIndex(request).getProject();
        showProject(project);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
