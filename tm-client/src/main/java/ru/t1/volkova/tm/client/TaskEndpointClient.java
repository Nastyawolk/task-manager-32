package ru.t1.volkova.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.endpoint.*;
import ru.t1.volkova.tm.dto.response.task.*;
import ru.t1.volkova.tm.dto.request.task.*;

@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpoint {

    public TaskEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @Override
    public @NotNull TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @Override
    public @NotNull TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @Override
    public @NotNull TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @Override
    public @NotNull TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @Override
    public @NotNull TaskClearResponse clearTask(@NotNull final TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @Override
    public @NotNull TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @Override
    public @NotNull TaskGetByIdResponse getTaskById(@NotNull final TaskGetByIdRequest request) {
        return call(request, TaskGetByIdResponse.class);
    }

    @Override
    public @NotNull TaskGetByIndexResponse getTaskByIndex(@NotNull final TaskGetByIndexRequest request) {
        return call(request, TaskGetByIndexResponse.class);
    }

    @Override
    public @NotNull TaskListByProjectIdResponse getTaskByProjectId(@NotNull final TaskListByProjectIdRequest request) {
        return call(request, TaskListByProjectIdResponse.class);
    }

    @Override
    public @NotNull TaskListResponse listTask(@NotNull final TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @Override
    public @NotNull TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @Override
    public @NotNull TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @Override
    public @NotNull TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @Override
    public @NotNull TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

}
