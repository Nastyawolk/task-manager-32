package ru.t1.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Start project by index.";

    @NotNull
    private static final String NAME = "project-start-by-index";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(index, Status.IN_PROGRESS);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
