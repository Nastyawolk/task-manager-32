package ru.t1.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.user.UserRegistryRequest;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "Registry user";

    @NotNull
    private static final String NAME = "user-registry";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(login, password, email);
        @Nullable final User user = getUserEndpoint().registryUser(request).getUser();
        showUser(user);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
