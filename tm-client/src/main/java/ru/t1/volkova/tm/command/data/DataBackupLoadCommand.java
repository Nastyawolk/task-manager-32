package ru.t1.volkova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.data.DataBackupLoadRequest;
import ru.t1.volkova.tm.enumerated.Role;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load backup from file";

    @NotNull
    public static final String NAME = "backup-load";

    @SneakyThrows
    @Override
    public void execute() {
        serviceLocator.getDomainEndpoint().loadDataBackup(new DataBackupLoadRequest());
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
