package ru.t1.volkova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.response.AbstractUserResponse;
import ru.t1.volkova.tm.model.User;

@NoArgsConstructor
public final class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(@NotNull final User user) {
        super(user);
    }

}
