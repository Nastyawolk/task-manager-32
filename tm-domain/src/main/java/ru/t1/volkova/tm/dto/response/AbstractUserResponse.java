package ru.t1.volkova.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.User;

@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResultResponse {

    @Getter
    @Nullable
    private User user;

    public AbstractUserResponse(@Nullable final User user) {
        this.user = user;
    }

    public AbstractUserResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
