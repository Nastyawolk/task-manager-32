package ru.t1.volkova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public TaskGetByIndexRequest(@Nullable final Integer index) {
        this.index = index;
    }

}
